<?php


namespace App\Http\Repositories;


use App\Models\Image;
use App\Models\Product;
use Illuminate\Support\Facades\Storage;

class ImageRepository
{
    /**
     * @param int $productId
     * @return Image
     */

    public function store(int $productId): Image
    {
        $image = new Image();

        $image->product_id = $productId;
        $image->path = Storage::disk('local')->put('/image', request('image'));

        $image->save();

        return $image;
    }
}
