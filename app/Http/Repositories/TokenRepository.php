<?php


namespace App\Http\Repositories;


use App\Models\Token;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class TokenRepository
{
    /**
     * @param User $user
     * @return Token
     *
     */
    public function store(int $id): Token
    {
        $token = new Token();

        $token->token = Hash::make(time());

        $token->user_id = $id;

        $token->time_of_expires = Carbon::now()->addDay();

        $token->save();

        return $token;
    }
}
