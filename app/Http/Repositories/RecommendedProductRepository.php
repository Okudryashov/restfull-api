<?php


namespace App\Http\Repositories;


use App\Models\Product;
use App\Models\RecommendedProduct;
use phpDocumentor\Reflection\Types\Integer;

class RecommendedProductRepository
{
    /**
     * @param int $recommendedId
     * @param int $productId
     */
    public function store(int $recommendedId, int $productId): void
    {
            $recommended = new RecommendedProduct();

            $recommended->product_id = $productId;
            $recommended->recommended_id = $recommendedId;

            $recommended->save();

    }
}
