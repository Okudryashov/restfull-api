<?php


namespace App\Http\Repositories;


use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserRepository
{
    /**
     * @param string $email
     * @param string $password
     * @param string $name
     * @param string $phone
     * @return User
     */
    public function create(string $email, string $password, string $name, string $phone): User
    {

        $user = new User;

        $user->name = $name;
        $user->email = $email;
        $user->password = Hash::make($password);
        $user->phone = $phone;

        $user->save();

        return $user;
    }
}
