<?php


namespace App\Http\Repositories;

use App\Models\Category;

class CategoryRepository
{
    /**
     * @param string $category
     * @return Category
     */
    public function store(string $category): Category
    {
        $category = new Category();

        $category->title = $category;

        $category->save();

        return $category;
    }
}
