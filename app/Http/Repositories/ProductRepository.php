<?php


namespace App\Http\Repositories;
use App\Http\Requests\ProductUpdateRequest;
use App\Models\Product;
use Illuminate\Database\Eloquent\Collection;

class ProductRepository
{
    /**
     * @var ImageRepository
     */
    protected $imageRepository;

    protected $recommendedProductRepository;

    /**
     * ProductRepository constructor.
     * @param ImageRepository $imageRepository
     * @param RecommendedProductRepository $recommendedProductRepository
     */
    public function __construct(ImageRepository $imageRepository, RecommendedProductRepository $recommendedProductRepository)
    {
        $this->imageRepository = $imageRepository;
        $this->recommendedProductRepository = $recommendedProductRepository;
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return Product::all();
    }

    /**
     * @param int $id
     * @return Product
     */
    public function show(int $id): Product
    {
        return Product::find($id);
    }

    /**
     * @param string $title
     * @param string $description
     * @param int $categoryId
     * @param array $recommendedIds
     * @return Product
     */
    public function store(string $title, string $description, int $categoryId, array $recommendedIds): Product
    {
        $product = new Product();

        $product->title = $title;
        $product->description = $description;
        $product->category_id = $categoryId;

        $product->save();

        if (request('image')) {
            $this->imageRepository->store($product->id);
        }

//        todo: utilize queses below

        if ($recommendedIds === []) {
            return $product;
        }

        foreach ($recommendedIds as $recommendedId) {
            $this->recommendedProductRepository->store($recommendedId, $product->id);
        }

        return $product;
    }

    /**
     * @param Product $product
     * @param string|null $title
     * @param string|null $description
     * @return Product
     */
    public function renew(Product $product, string $title = null, string $description = null): Product
    {
        if ($title) {
            $product->title = $title;
        }

        if ($description) {
            $product->description = $description;
        }

        $product->save();

        return $product;
    }

    /**
     * @param Product $product
     * @throws \Exception
     */
    public function destroy(Product $product): void
    {
        $product->delete();
    }
}
