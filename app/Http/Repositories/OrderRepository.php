<?php


namespace App\Http\Repositories;


use App\Models\Order;
use App\Models\OrderProduct;

class OrderRepository
{
    /**
     * @param array $productsId
     * @param int $user_id
     * @return Order
     */
    public function store(array $productsId, int $user_id): Order
    {
        $order = new Order();

        $order->user_id = $user_id;

        $order->save();

        foreach ($productsId as $productId) {
            $orderProduct = new OrderProduct();

            $orderProduct->order_id = $order->id;
            $orderProduct->product_id = $productId;

            $orderProduct->save();

        }

        return $order;

    }

    /**
     * @param Order $order
     * @return Order
     */
    public function completeOrder(Order $order): Order
    {
        $order->is_completed = true;

        $order->save();

        return $order;
    }

    /**
     * @param int $id
     * @return Order
     */
    public function show(int $id): Order
    {
        return Order::find($id);
    }

    /**
     * @param Order $order
     * @throws \Exception
     */
    public function destroy(Order $order): void
    {
        $order->delete();
    }
}
