<?php

namespace App\Http\Controllers;

use App\Http\Repositories\OrderRepository;
use App\Http\Resources\OrderResource;
use App\Http\Requests\OrderCreateRequest;
use App\Mail\OrderShipped;
use App\Models\Order;
use App\Models\User;
use Illuminate\Contracts\Mail;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OrderController extends Controller
{
    protected $repository;

    /**
     * OrderController constructor.
     * @param OrderRepository $repository
     */
    public function __construct(OrderRepository $repository)
    {
        $this->repository = $repository;
        $this->middleware('is.auth');
        $this->middleware('is.admin');
    }

    /**
     * @param OrderCreateRequest $request
     * @param User $user
     * @return OrderResource
     */
    public function create(OrderCreateRequest $request, User $user): OrderResource
    {
        $order = $this->repository->store(
            $request->productsId,
            $user->id
        );

        $this->ship($order);

        return new OrderResource($order);
    }

    /**
     * @param Order $order
     */
    public function ship(Order $order): void
    {
        Mail::to($order->user->email)->send(new OrderShipped($order));
    }

    /**
     * @param Order $order
     * @return OrderResource
     */
    public function complete(Order $order): OrderResource
    {
        $order = $this->repository->completeOrder($order);

        return new OrderResource($order);
    }

    /**
     * @param Order $order
     * @return OrderResource
     */
    public function show(Order $order): OrderResource
    {
        $order = $this->repository->show($order->id);

        return new OrderResource($order);
    }

    /**
     * @param Order $order
     * @return Response
     * @throws \Exception
     */
    public function delete(Order $order): Response
    {
        if (! $order->is_completed) {
            return response('You can not delete uncompleted order', '406');
        }

        $this->repository->destroy($order);

        return response('The order was deleted successfully', '200');
    }
}
