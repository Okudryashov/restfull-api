<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductCreateRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Http\Resources\ProductResourceCollection;
use App\Models\Product;
use App\Http\Repositories\ProductRepository;
use App\Http\Resources\ProductResource;
use Illuminate\Http\Response;

class ProductController extends Controller
{
    /**
     * @var ProductRepository
     */
    protected $repository;

    /**
     * ProductController constructor.
     * @param ProductRepository $repository
     */
    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
        $this->middleware('is.auth')->except('index', 'show');
        $this->middleware('is.admin')->except( 'index', 'show');
    }

    /**
     * @return ProductResourceCollection
     */
    public function index(): ProductResourceCollection
    {
        $products = $this->repository->all();

        return new ProductResourceCollection($products);
    }

    /**
     * @param ProductCreateRequest $request
     * @return ProductResource
     */
    public function create(ProductCreateRequest $request): ProductResource
    {
        $product = $this->repository->store(
            $request->title,
            $request->description,
            $request->categoryId,
            $request->recommended
        );

        return new ProductResource($product);
    }

    /**
     * @param Product $product
     * @return ProductResource
     */
    public function show(Product $product): ProductResource
    {
        $product = $this->repository->show($product->id);

        return new ProductResource($product);
    }

    /**
     * @param Product $product
     * @param ProductUpdateRequest $request
     * @return ProductResource
     */
    public function update(Product $product, ProductUpdateRequest $request): ProductResource
    {
        $product = $this->repository->renew($product,
            $request->title,
            $request->description
        );

        return new ProductResource($product);
    }

    /**
     * @param Product $product
     * @return Response
     * @throws \Exception
     */
    public function delete(Product $product): Response
    {
        $this->repository->destroy($product);

        return response('', 200);
    }

}
