<?php

namespace App\Http\Controllers;

use App\Http\Repositories\CategoryRepository;
use App\Http\Resources\CategoryResource;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryCreateRequest;


class CategoryController extends Controller
{
    protected $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
        $this->middleware('is.admin');
    }

    /**
     * @param CategoryCreateRequest $request
     * @return CategoryResource
     */
    public function create(CategoryCreateRequest $request): CategoryResource
    {
        $category = $this->repository->store($request->category);

        return new CategoryResource($category);
    }
}
