<?php

namespace App\Http\Controllers;

use App\Http\Repositories\TokenRepository;
use App\Http\Requests\LoginRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Models\Token;
use mysql_xdevapi\SqlStatementResult;

class LoginController extends Controller
{

    private $repository;

    public function __construct(TokenRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * @param LoginRequest $request
     * @return UserResource|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function login(LoginRequest $request)
    {
        $user = User::where('email', $request->email)->first();

        if (! $user) {
            return response('User not found ', '404');
        }

        if (! Hash::check('password', $request->password)) {
            return response('Password is incorrect ', '406');
        }

        $this->repository->store($user->id);

        return new UserResource($user);
    }


}
