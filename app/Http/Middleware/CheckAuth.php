<?php

namespace App\Http\Middleware;

use App\Models\Token;
use Carbon\Carbon;
use Closure;

class CheckAuth
{
    /**
     *
     *
     * @param $request
     * @param Closure $next
     * @return mixed
     * @throws \Exception
     */

    public function handle($request, Closure $next)
    {
        $token = $request->header('Auth-token');


        if (!$token) {
            return response('Unauthorized ', '401');
        }


        $tokenExist = Token::where('token', $token)->first();


        if (!$tokenExist) {
            return response('Token is not valid', '401');
        }


        if ($tokenExist->time_of_expires < Carbon::now()) {
            return response('Time expired', '401');
        }

        return $next($request);
    }
}
