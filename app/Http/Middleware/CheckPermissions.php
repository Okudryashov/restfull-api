<?php

namespace App\Http\Middleware;

use App\Models\Token;
use Closure;

class CheckPermissions
{
    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {

        $token = $request->header('Auth-token');

        if (!$token) {
            return response('Unauthorized ', '401');
        }

        $tokenExist = Token::where('token', $token)->first();

        if (!$tokenExist) {
            return response('Token is not valid', '401');
        }

        if ($token->user->is_admin === false) {

            return response('Forbidden', '403');

        }

        return $next($request);
    }
}
