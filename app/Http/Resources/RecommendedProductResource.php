<?php

namespace App\Http\Resources;

use App\Models\RecommendedProduct;
use Illuminate\Http\Resources\Json\JsonResource;

class RecommendedProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /**
         * @var RecommendedProduct $recommended
         */

        $recommended = $this;

        return [
            'id' => $recommended->product->id,
            'title' => $recommended->product->title,
            'description' => $recommended->product->description,
            'image' => new ImageResource($recommended->product->image),
        ];
    }
}
