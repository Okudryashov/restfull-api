<?php

namespace App\Http\Resources;

use App\Models\Order;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /**
         * @var Order $order
         */

        $order = $this;


        return [
            'order' => $order->id,
            'is_completed' => $order->is_completed,
            'user' =>  UserResource::make($order->user),
            'products' => new ProductResourceSmallCollection($order->products)
        ];
    }
}
