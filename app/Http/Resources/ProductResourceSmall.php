<?php

namespace App\Http\Resources;

use App\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResourceSmall extends JsonResource
{
    /**
     * Transform the resource into an array.
     * Use when do not need to include recommendedProducts
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        /**
         * @var Product $product
         */

        $product = $this;

        return [
            'title' => $product->title,
            'description' => $product->description,
        ];
    }
}
