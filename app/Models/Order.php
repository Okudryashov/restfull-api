<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Order
 * @package App\Models
 *
 * @property int $id
 * @property int $user_id
 * @property boolean $is_completed
 */
class Order extends Model
{
    protected $fillable = ['user_id', 'is_completed'];

    public function products()
    {
        return $this->belongsToMany('App\Models\Product', 'order_products', 'order_id', 'product_id');
    }


    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
