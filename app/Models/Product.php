<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @package App\Models
 *
 * @property integer $id
 * @property integer $category_id
 * @property string title;
 * @property string $description;
 * @method public recommendedProducts()
 */
class Product extends Model
{
    protected $fillable = ['category_id', 'title', 'description'];

    public function recommended()
    {
        return $this->hasMany('App\Models\RecommendedProduct');
    }

    public function image()
    {
        return $this->hasOne('App\Models\Image');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

}
