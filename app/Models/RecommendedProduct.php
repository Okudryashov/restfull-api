<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RecommendedProduct
 * @package App\Models
 *
 * @property int $product_id
 * @property int $recommended_id
 */
class RecommendedProduct extends Model
{
    protected $fillable = ['product_id', 'recommended_id'];

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'recommended_id');
    }
}

