<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Image
 * @package App\Models
 *
 * @property int $id
 * @property int $product_id
 * @property string $path
 */
class Image extends Model
{
    protected $fillable = ['product_id', 'path'];

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }
}
