<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Token
 * @package App\Models
 *
 * @property int $user_id
 * @property string $token
 * @property string $time_of_expires
 */
class Token extends Model
{
    protected $fillable = ['user_id', 'token', 'time_of_expires'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
