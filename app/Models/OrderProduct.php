<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Order_Product
 * @package App\Models
 *
 * @property int $order_id
 * @property int $product_id
 */
class OrderProduct extends Model
{
    protected $fillable = ['order_id', 'product_id'];

}
