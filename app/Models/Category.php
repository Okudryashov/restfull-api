<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * @package App\Models
 *
 * @property string $title
 */
class Category extends Model
{
    protected $fillable = ['title'];

    public function product()
    {
        return $this->hasMany('App\Models\Product');
    }
}
