<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Order</title>
</head>
<body>
<div>
    @foreach($order as $item)
        Product: {{ $item->product->name }}
        Customer: {{ $item->user->email }}
        Number: {{ $item->number }}
    @endforeach
</div>
</body>
</html>
