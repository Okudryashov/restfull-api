<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Email shipper</title>
</head>
<body>
    <div>
        {{ $userEmail }}
    </div>

    <div>
        {{ $userName }}
    </div>

    @foreach($products as $product)
        <div>
            {{ $product->name }}
            {{ $product->category()->name }}
        </div>

        <div>
            <img src="{{ $product->image()->path }}">
        </div>
    @endforeach
</body>
</html>
