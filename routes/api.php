<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//DONE: роутер лучше делать так
// Route::group(['prefix' => 'products'], function() {
//     Route::get('/', 'ProductController@index');
//     Route::post('/', 'ProductController@create');
//     Route::group(['prefix' => '{product}'], function() {
//         Route::get('/', 'ProductController@show');
//         Route::put('/', 'ProductController@update');
//         Route::delete('/', 'ProductController@delete');
//     });
// });
// Такой формат более понятный и сгрупированный
//

Route::group(['prefix' => 'products'], function() {
    Route::get('/', 'Productcontroller@index');
    Route::post('/', 'ProductController@create');
    Route::group(['prefix' => '{product}'], function() {
        Route::get('/', 'ProductController@show');
        Route::put('/', 'ProductController@update');
        Route::delete('/', 'ProductController@delete');
    });
});

Route::group(['prefix' => 'order'], function() {
    Route::post('/{user}', 'OrderController@create');
    Route::group(['prefix' => '{order}'], function() {
        Route::get('/', 'OrderController@show');
        Route::put('/', 'OrderController@complete');
        Route::delete('/', 'OrderController@delete');
    });
});



Route::post('/register', 'RegisterController@create');
Route::get('/login', 'LoginController@login');
