<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Product;
class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1,20) as $index) {
            Product::create([
                'name' => $faker->word(),
                'description' => $faker->sentence(3),
                'number' => $faker->numberBetween(1,100)
            ]);
        }
    }
}
